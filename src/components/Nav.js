import styled from 'styled-components'

import {primary} from '../styles/pallete'

const Nav = styled.nav`
  margin: 60px 0;
  font-size: 24px;
  color: ${primary};
`

export default Nav;